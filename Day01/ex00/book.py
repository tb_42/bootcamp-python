import datetime
import time
from recipe import Recipe


class Book:
    def __init__(self, name):
        self.name = name
        self.creation_date = datetime.datetime.now()
        self.last_update = datetime.datetime.now()
        self.recipe_list = {'starter': {}, 'lunch': {}, 'dessert': {}}

    def get_recipe_by_name(self, name):
        for key in self.recipe_list:
            for value in self.recipe_list[key]:
                if name == value.name:
                    print(value.description)
                    return 0
        print("This recipe is not in the book")
        return 0

    def get_recipe_by_types(self, recipe_type):
        if not (recipe_type == 'starter'
                or recipe_type == 'lunch' or recipe_type == 'dessert'):
            print("Recipe type must be 'starter', 'lunch', or 'dessert'")
            return 0
        for value in self.recipe_list[recipe_type]:
            print(value.name)
            return 0

    def add_recipe(self, recipe):
        if not isinstance(recipe, Recipe):
            print("This is not a recipe")
        if recipe not in self.recipe_list:
            self.recipe_list[recipe.recipe_type][recipe] = recipe
            self.last_update = datetime.date.today()
