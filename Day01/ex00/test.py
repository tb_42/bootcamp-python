from book import Book
from recipe import Recipe
from datetime import datetime

tourte = Recipe('Tourte', 4, 60, ['farine', 'oeufs', 'poisson'], 'lunch')
to_print = str(tourte)
# print(to_print)

cookbook = Book('cookbook')
cookbook.add_recipe(tourte)
cookbook.get_recipe_by_types('lunch')
cookbook.get_recipe_by_name('Tourte')
