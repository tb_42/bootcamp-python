class Recipe:
    def __init__(self, name, cooking_lvl,
                cooking_time, ingredients, recipe_type):
        if len(name) == 0:
            print("Invalid name")
            exit()
        if not isinstance(cooking_lvl, int) or\
                cooking_lvl < 1 or cooking_lvl > 5:
            print("Level cooking must be an integer between 1 and 5")
            exit()
        if not isinstance(cooking_time, int) or cooking_time < 0:
            print("Cooking time must be an integer")
            exit()
        if not isinstance(ingredients, list):
            print("Ingredients must be a list of strings")
            exit()
        if not (recipe_type == 'starter' or
        recipe_type == 'lunch' or recipe_type == 'dessert'):
            print("Recipe type must be 'starter', 'lunch', or 'dessert'")
            exit()
        self.name = name
        self.cooking_lvl = cooking_lvl
        self.cooking_time = cooking_time
        self.ingredients = ingredients
        self.recipe_type = recipe_type
        self.description = "name : " + name\
                            + "\nCooking level : " + str(cooking_lvl)\
                            + "\nCooking time : " + str(cooking_time)\
                            + " minutes" + "\nRecipe type : "\
                            + recipe_type + "\nList of ingredients : "
        for i in ingredients:
            self.description += i + ' '

    def __str__(self):
        txt = self.description
        return txt
