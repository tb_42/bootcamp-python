class Vector:
    def __init__(self, list_values):
        self.list_values = []
        if list_values == 0:
            pass
        elif isinstance(list_values, list):
            self.list_values = list_values
            for i in list_values:
                if not (isinstance(i, int) or isinstance(i, float)):
                    print("ERROR invalid input")
                    exit()
        elif isinstance(list_values, int):
            for i in range(0, list_values):
                self.list_values.append(i)
        elif isinstance(list_values, tuple) and len(list_values) == 2:
            self.list_values = []
            for i in range(list_values[0], list_values[1]):
                self.list_values.append(i)
        else:
            print("ERROR invalid input")
            exit()
        for i in self.list_values:
            i = float(i)
        self.size = len(self.list_values)

    def __add__(self, other):
        new = Vector(0)
        if self.size != other.size:
            print("ERROR : Vectors must have same number of elements")
            exit()
        if not isinstance(other, Vector) or not isinstance(self, Vector):
            print("ERROR : Arguments must be vectors")
        for i in range(self.size):
            new.list_values.append(self.list_values[i] + other.list_values[i])
        new.size = len(new.list_values)
        return new

    def __radd__(self, other):
        return other.__add__(self)

    def __sub__(self, other):
        new = Vector(0)
        if self.size != other.size:
            print("ERROR : Vectors must have same number of elements")
            exit()
        if not isinstance(other, Vector) or not isinstance(self, Vector):
            print("ERROR : Arguments must be vectors")
        for i in range(self.size):
            new.list_values.append(self.list_values[i] - other.list_values[i])
        new.size = len(new.list_values)
        return new

    def __rsub__(self, other):
        return other.__sub__(self)

    def __truediv__(self, other):
        new = Vector(0)
        if other == 0:
            print("ERROR : Division by zero")
            exit()
        if (not isinstance(other, float)) and (not isinstance(other, int)):
            print("ERROR : Argument 2 must be a number")
            exit()
        if not isinstance(self, Vector):
            print("ERROR : Argument 1 must be a vector")
            exit()
        for i in range(self.size):
            new.list_values.append(self.list_values[i] / other)
        new.size = len(new.list_values)
        return new

    def __rtruediv__(self, other):
        return other.__truediv__(self)

    def __mul__(self, other):
        if isinstance(other, int) or isinstance(other, float):
            new = Vector(0)
            for i in range(self.size):
                new.list_values.append(self.list_values[i] * other)
            new.size = len(new.list_values)
        elif isinstance(other, Vector):
            if self.size != other.size:
                print("ERROR : Vectors must have same number of elements")
                exit()
            new = 0
            for i in range(self.size):
                new += self.list_values[i] * other.list_values[i]
        else:
            print("Argument 2 must be a number or a vector")
            exit()
        return new

    def __rmul__(self, other):
        return other.__mul__(self)

    def __str__(self):
        ch = ""
        for i in range(self.size):
            ch += str(self.list_values[i]) + ', '
        ch = '[' + ch[:-2] + ']'
        return ch

    def __repr__(self):
        return "%s(%r)" % (self.__class__, self.__dict__)


vec0 = Vector(5)
print('vec0 = Vector(5)')
print('vec0 = ', end='')
print(vec0)
vec = Vector((10, 13))
print('vec = Vector((10, 13))')
print('vec = ', end='')
print(vec)
vec2 = Vector([1, 2, 3])
print('vec2 = Vector([1, 2, 3])')
print('vec2 = ', end='')
print(vec2)
vec3 = vec * 3
print('vec3 = vec * 3')
print('vec3 = ', end='')
print(vec3)
vec4 = vec / 4
print('vec4 = vec / 4')
print('vec4 = ', end='')
print(vec4)
