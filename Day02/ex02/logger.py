import time
import os
from random import randint

funct = {'start_machine': "Start Machine", 'boil_water': "Boil Water", 
		'make_coffee': "Make Coffee", 'add_water': "Add Water"}

def log(func):
	def inner(*args, **kwargs):
		b = 0
		st = time.time()
		a = func(*args, **kwargs)
		st2 = time.time()
		st3 = st2 - st
		st3 *= 1000
		if st3 > 1000:
			st3 /= 1000
			b = 1
		file = open("machine.log", 'a')
		file.write('(' + os.getlogin() + ')' + 'Running: ' + funct[func.__name__] 
					+ '\t' + '[ exec-time = ' + ("%.3f" % st3) 
					+ (' s  ]' if b == 1 else ' ms ]') + '\n')
		return a
	return inner

class CoffeeMachine():

	water_level = 100
	
	@log
	def start_machine(self):
		if self.water_level > 20:
			return True
		else:
			print("Please add water!")
			return False

	@log
	def boil_water(self):
		return "boiling..."

	@log
	def make_coffee(self):
		if self.start_machine():
			for _ in range(20):
				time.sleep(0.1)
				self.water_level -= 1
			print(self.boil_water())
			print("Coffee is ready!")

	@log
	def add_water(self, water_level):
		time.sleep(randint(1, 5))
		self.water_level += water_level
		print("Blub blub blub...")


if __name__ == "__main__":

	machine = CoffeeMachine()
	for i in range(0, 5):
		machine.make_coffee()

	machine.make_coffee()
	machine.add_water(70)