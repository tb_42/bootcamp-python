def ft_reduce(function_to_apply, list_of_inputs):
    ret = list_of_inputs[0]
    for i in list_of_inputs[1:]:
        ret = function_to_apply(ret, i)
    return ret

# def ft_sum(a, b):
#     return a + b

# print(ft_reduce(ft_sum, [1, 2, 3, 4, 5, 6, 7, 8]))