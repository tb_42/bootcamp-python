def ft_map(function_to_apply, list_of_inputs):
    liste = []
    for i in range(len(list_of_inputs)):
        liste.append(function_to_apply(list_of_inputs[i]))
    return liste

# def double(input):
#     return 2 * input

# a = ft_map(double, [1, 3, 9, 15])
# print(a)