def ft_filter(function_to_apply, list_of_inputs):
    liste = []
    for i in range(len(list_of_inputs)):
        if function_to_apply(list_of_inputs[i]) == True:
            liste.append(list_of_inputs[i])
    return liste

# def ft_even(i):
#     if i % 2 == 0:
#         return True
#     return False

# l = [1, 2, 3, 4, 5, 6, 7, 8]
# print(ft_filter(ft_even, l))