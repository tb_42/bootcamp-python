languages = {
    'Python': 'Guido van Rossum',
    'Ruby': 'Yukihiro Matsumoto',
    'PHP': 'Rasmus Lerdorf',
    }

str1 = "Python was created by %s" % languages["Python"]
str2 = "Ruby was created by %s" % languages["Ruby"]
str3 = "PHP was created by %s" % languages["PHP"]
print(str1)
print(str2)
print(str3)
