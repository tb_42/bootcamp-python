import sys

if len(sys.argv) > 1:
    c = sys.argv[1]
    if c[0] == '-':
        c = c[1:len(sys.argv[1]) - 1]
if len(sys.argv) == 2 and c and c.isdigit():
    i = int(sys.argv[1])
    if i == 0:
        print("I'm zero")
    elif i % 2 == 0:
        print("I'm even")
    else:
        print("I'm odd")
else:
    print("ERROR")
