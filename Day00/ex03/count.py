import string


def text_analyzer(*arg):
    i = 0
    upc = 0
    lowc = 0
    punc = 0
    spac = 0
    if len(arg) <= 1:
        if len(arg) == 0:
            print("What is the text to analyse")
            str = input()
        else:
            str = arg[0]
        while i < len(str):
            if str[i] in string.ascii_lowercase:
                lowc += 1
            elif str[i] in string.ascii_uppercase:
                upc += 1
            elif str[i] == ' ':
                spac += 1
            elif str[i] in string.punctuation:
                punc += 1
            i += 1
        print("The text contain", i, "characters:")
        print()
        print(upc, "upper letters")
        print()
        print(lowc, "lower letters")
        print()
        print(punc, "punctuation marks")
        print()
        print(spac, "spaces")
    elif len(arg) >= 2:
        print("ERROR")
