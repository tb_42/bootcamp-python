import sys

if len(sys.argv) == 3:
    try:
        x = int(sys.argv[1])
        y = int(sys.argv[2])
    except ValueError:
        print("InputError: only numbers\n")
        print("Usage: python operations.py <number1> <number2>\
        \nExample:\n\tpython operations.py 10 3")
        exit()
    print("Sum:\t\t", x + y)
    print("Difference:\t", x - y)
    print("Product:\t", x * y)
    if y != 0:
        print("Quotient:\t", x / y)
        print("Remainder:\t", x % y)
    else:
        print("Quotient:\tERROR (div by zero)")
        print("Remainder:\tERROR (modulo by zero)")
elif len(sys.argv) == 2:
    print("InputError: too few arguments\n")
    print("Usage: python operations.py <number1> <number2>\
    \nExample:\n\tpython operations.py 10 3")
elif len(sys.argv) > 3:
    print("InputError: too many arguments\n")
    print("Usage: python operations.py <number1> <number2>\
    \nExample:\n\tpython operations.py 10 3")
