cookbook = {'sandwich': {'ingredients': [
        'ham', 'bread', 'cheese', 'tomatoes'],
        'meal': 'lunch', 'prep_time': 10}, 'cake': {
        'ingredients': ['flour', 'sugar', 'eggs'],
        'meal': 'dessert',
        'prep_time': 60},
        'salad': {
        'ingredients': [
        'avocado', 'arugula', 'tomatoes', 'spinach'],
        'meal': 'lunch', 'prep_time': 15}}


def print_recipe(ch):
    if ch in cookbook:
        print('Recipe of', ch, ':', cookbook[ch]['ingredients'])
        print('To eat for', cookbook[ch]['meal'])
        print('Time of preparation :', cookbook[ch]['prep_time'], 'minutes')
        return 1


def del_recipe(ch):
    if ch in cookbook:
        del(cookbook[ch])
        return 1


def add_recipe(name, ingredients, lunch, prep_time):
    if name not in cookbook:
        cookbook[name] = {}
        cookbook[name]['ingredients'] = list(ingredients)
        cookbook[name]['meal'] = lunch
        cookbook[name]['prep_time'] = prep_time


def print_names():
    for key in cookbook.keys():
        print("Recette :", key)


def cook_prog():
    print('\nPlease select an option by typing the corresponding number:')
    print('1: Add a recipe')
    print('2: Delete a recipe')
    print('3: Print a recipe')
    print('4: Print the cookbook')
    print('5: Quit\n')
    command = input()
    if command.isdigit:
        if int(command) == 1:
            print('\nPlease enter the name of the recipe\n')
            str1 = input()
            print('Please enter all the ingredients\n')
            str2 = list(input().split(' '))
            print('Please enter the type of the lunch\n')
            str3 = input()
            print('Please enter the time of preparation\n')
            str4 = int(input())
            add_recipe(str1, str2, str3, str4)
            cook_prog()
        if int(command) == 2:
            print('\nPlease enter the name of the recipe to delete\n')
            if del_recipe(input()) != 1:
                print("This recipe does not exist")
            else:
                print('Recipe successfully deleted')
            cook_prog()
        if int(command) == 3:
            print('\nWhich recipe ?\n')
            if print_recipe(input()) != 1:
                print("This recipe does not exist")
            cook_prog()
        if int(command) == 4:
            print()
            print_names()
            cook_prog()
        if int(command) == 5:
            exit()
    else:
        print('This is not a valid command\n')
        cook_prog()


cook_prog()
