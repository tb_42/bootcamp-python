import sys
ac = len(sys.argv) - 1
while ac > 0:
    p = len(sys.argv[ac])
    while p >= 1:
        if sys.argv[ac][p - 1] == (sys.argv[ac][p - 1]).lower():
            print((sys.argv[ac][p - 1]).upper(), end='')
        else:
            print((sys.argv[ac][p - 1]).lower(), end='')
        p -= 1
    ac -= 1
    if ac > 0:
        print(" ", end='')
    else:
        print()