from ImageProcessor import ImageProcessor
from PIL import Image
from numpy import asarray
import numpy as np

class ScrapBooker():

    def crop(self, array, dimensions, position):
        img = Image.fromarray(array)
        if dimensions[0] > img.size[0] or dimensions[1] > img.size[1]:
            print("Dimensions too big")
            exit()
        box = (position[0], position[1], (position[0] 
             + dimensions[0]), (position[1] + dimensions[1]))
        img = img.crop(box)
        return(asarray(img))

    def thin(self, array, n, axis):
        return np.delete(array, list(range(0, array.shape[0], n)), axis)
    
    def juxtapose(self, array, n, axis):
        ar2 = array
        for _ in range(n - 1):
            ar2 = np.concatenate((ar2, array), axis)
        return ar2
    
    def mosaic(self, array, dimensions):
        sb = ScrapBooker()
        ar2 = sb.juxtapose(array, dimensions[0], 0)
        ar2 = sb.juxtapose(ar2, dimensions[1], 1)
        return ar2

ip = ImageProcessor()
ab = ip.load('mushroom.png')
new = ScrapBooker()
pp = new.mosaic(ab, (3,4))
image = Image.fromarray(pp)
image.show()