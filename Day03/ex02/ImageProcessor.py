from PIL import Image
from numpy import asarray

class ImageProcessor():

    def load(self, path):
        image = Image.open(path)
        data = asarray(image)
        print(image.size)
        return(data)

    def display(self, array):
        image = Image.fromarray(array)
        image.show()

# ip = ImageProcessor()
# ab = ip.load('mushroom.png')
# ip.display(ab)