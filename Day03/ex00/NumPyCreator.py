import numpy

class NumPyCreator():

    def from_list(self, liste):
        return numpy.array(liste)
    
    def from_tuple(self, tupl):
        return numpy.array(tupl)

    def from_iterable(self, liste):
        return numpy.array(liste)
    
    def from_shape(self, dim):
        return numpy.zeros(dim)
    
    def random(self, dim):
        return numpy.random.rand(dim[0], dim[1])
    
    def identity(self, dim):
        return numpy.identity(dim)