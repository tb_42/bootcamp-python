import pandas

class FileLoader:

    def load(self, path):
        p = pandas.read_csv(path)
        print(p.shape)
        return p

    def display(self, df, n):
        print(df.head(n)) if n > 0 else print(df.tail(-n))


fl = FileLoader()
data = fl.load('airtravel.csv')
print('\ndataframe :\n')
print(data)
print('\n5 first :\n')
fl.display(data, 5)
print('\n5 last :\n')
fl.display(data, -5)