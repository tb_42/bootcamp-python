from FileLoader import FileLoader
import pandas

def youngestfellah(df, year):
    yfilter = df['Year']==2004
    d = df[yfilter]
    mfilter = d['Sex']=='M'
    ffilter = d['Sex']=='F'
    dm = d[mfilter]
    df = d[ffilter]
    minm = dm.min()['Age']
    minf = df.min()['Age']
    dic = {'f': minf, 'm': minm}
    print(dic)

fl = FileLoader()
data = fl.load('../athletes.csv')
youngestfellah(data, 2004)