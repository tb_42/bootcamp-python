import pandas

class FileLoader:

    def load(self, path):
        p = pandas.read_csv(path)
        print(p.shape)
        return p

    def display(self, df, n):
        print(df.head(n)) if n > 0 else print(df.tail(-n))