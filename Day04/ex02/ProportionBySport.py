from FileLoader import FileLoader
import pandas

def ProportionBySport(df, year, sport, gender):
    yfilter = df['Year']==year
    d = df[yfilter]
    gfilter = d['Sex']==gender
    dg = d[gfilter]
    dg = dg.drop_duplicates(subset="Name")
    ng = dg.shape[0]
    sfilter = dg['Sport']==sport
    ds = dg[sfilter]
    ns = ds.shape[0]
    return ns / ng

fl = FileLoader()
data = fl.load('../athletes.csv')
print(ProportionBySport(data, 2004, 'Tennis', 'F'))