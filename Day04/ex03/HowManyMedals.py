from FileLoader import FileLoader
import pandas

def howManyMedals(df, name):
    nfilter = df['Name']==name
    d = df[nfilter]
    dic = {}
    for index, rows in d.iterrows():
        if rows['Year'] not in dic:
            dic[rows['Year']] = {'G' : 0, 'S' : 0, 'B' : 0}
            if not pandas.isna(rows['Medal']):
                letter = rows['Medal']
                dic[rows['Year']][letter[0]] += 1
        else:
            if not pandas.isna(rows['Medal']):
                letter = rows['Medal']
                dic[rows['Year']][letter[0]] += 1
    return dic

fl = FileLoader()
data = fl.load('../athletes.csv')
print(howManyMedals(data, 'Kjetil Andr Aamodt'))